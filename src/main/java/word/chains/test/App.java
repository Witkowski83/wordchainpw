package word.chains.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
    	MagicalSolutionsBox box = new MagicalSolutionsBox( loadDict(), 100 );
    	List<List<String>> combinations = box.findAllCombinationsNonRecurse("cat", "dog");
    	//List<List<String>> combinations = box.findAllCombinationsRecurse("ruby", "code");
    	System.out.println(combinations);
        
    }
    
    /**
     * loads the words dictionary from file
     * 
     * @return list of words defined in dictionary
     */
    public static String [] loadDict() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(App.class.getResourceAsStream("/wordlist.txt")))) {
            List<String> l = new ArrayList<>();
            String s;
            while ((s = reader.readLine()) !=null) {
                l.add(s);
            }
            return l.toArray(new String[l.size()]);
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }
}
