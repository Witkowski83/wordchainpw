package word.chains.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.apache.commons.lang3.StringUtils;

/**
 * Used to find solutions to word chains puzzle http://codekata.com/kata/kata19-word-chains/
 * @author witkowsp
 *
 */
public class MagicalSolutionsBox {

	private Set<String> dictSet;
	private Map<Integer, Set<WordWrapper>> preDivided = new HashMap<>();
	private int depth;
	
	public MagicalSolutionsBox(String[] dict, int depth) {
		this.dictSet = new HashSet<>(Arrays.asList(dict));
		this.depth = depth;
		init();
	}
	
	private void init() {
		for (String word: dictSet) {
			Integer lenght = word.length();
			Set<WordWrapper> wordSet = preDivided.get(lenght);
			if (wordSet == null) {
				wordSet = new HashSet<>();
			}
			wordSet.add(new WordWrapper(word));
			preDivided.put(lenght, wordSet);
		}
	}
	
	/**
	 * Finds one of the shortest solutions to the given word chain
	 * 
	 * @param startWord - initial word, algorithm start from this word
	 * @param endWord - final word - algorithm has to get to this word
	 *
	 * @return null if there's no solution, or the ordered list of words that lead to solution
	 */
	public List<String> findWordChain(String startWord, String endWord) {

		if (!validate(startWord, endWord)) {
			return null;
		}
		
		if (startWord.equals(endWord)) {
			return Arrays.asList(startWord);
		}

        for (int i=0; i < depth; i++) {
            Stack<String> stack = new Stack<>();
            recurse(startWord, endWord, stack, i);
            if (stack.size() != 1) {
            	return stack;
            }
        }
        
		return null;
	}
	
    private boolean recurse(String from, String to, Stack<String> stack, int maxDepth) {
        stack.push(from);
        if(stack.size() > maxDepth) {
        	return false;
        }
        
        if (from.equals(to)) {
        	return true;
        }

        for (WordWrapper word: preDivided.get(from.length())) {
            if (countLetterDifference(from, word.getWord() ) == 1 && !stack.contains(word)) {
                if (recurse(word.getWord(), to, stack, maxDepth)) {
                	return true;
                }
                stack.pop();
            }
        }
        return false;
    }
    
	/**
	 * Finds all possible solution to word chain puzzle, uses recursion
	 * 
	 * @param startWord - initial word, algorithm start from this word
	 * @param endWord - final word - algorithm has to get to this word
	 *
	 * @return null if there's no solution, list of solutions as ordered list of words
	 */
	public List<List<String>> findAllCombinationsRecurse(String startWord, String endWord) {
		if (!validate(startWord, endWord)) {
			return null;
		}
		
		if (startWord.equals(endWord)) {
			return Arrays.asList(Arrays.asList(startWord));
		}
        
        List<List<String>> combinations = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        recurseAllVariations(new WordWrapper(startWord), endWord, combinations, stack);
        
        return combinations;
	}
	
	private void recurseAllVariations(WordWrapper startWord, String endWord, List<List<String>> combinations, Stack<String> currentPath) {
		
		currentPath.push(startWord.getWord());
        if (startWord.equals(endWord)) {
        	List<String> solution = new ArrayList<>();
        	solution.addAll(currentPath);
        	combinations.add(solution);
        	return;
        }

        if (startWord.getSingleDifferenceWords() != null) {
        	for (WordWrapper w: startWord.getSingleDifferenceWords()) {
        		if (!currentPath.contains(w.getWord())) {
        			recurseAllVariations(w, endWord, combinations, currentPath);
	                currentPath.pop();
        		}
        	}
        } else {
        	
	        for (WordWrapper w: preDivided.get(startWord.getWord().length())) {
	            if (countLetterDifference(startWord.getWord(), w.getWord()) == 1 && !currentPath.contains(w.getWord())) {
	            	Set<WordWrapper> set = startWord.getSingleDifferenceWords();
	            	if (set == null) {
	            		set = new HashSet<>();
	            	}
	            	startWord.setSingleDifferenceWords(set);
	            	set.add(w);	            	
	            	recurseAllVariations(w, endWord, combinations, currentPath);	            	
	                currentPath.pop();
	            }
	        }
        }
	}
	
	/**
	 * Finds all shortest solutions to word chain puzzle, no recursion
	 * 
	 * @param startWord - initial word, algorithm start from this word
	 * @param endWord - final word - algorithm has to get to this word
	 *
	 * @return null if there's no solution, list of solutions as ordered list of words
	 */
	public List<List<String>> findAllCombinationsNonRecurse(String startWord, String endWord) {
		if (!validate(startWord, endWord)) {
			return null;
		}
		
		if (startWord.equals(endWord)) {
			return Arrays.asList(Arrays.asList(startWord));
		}
    
        return nonRecurse(new WordWrapper(startWord), endWord);
	}
	
	private List<List<String>> nonRecurse(WordWrapper startWord, String endWord) {
		List<List<String>> solutions = new ArrayList<>();
		Stack<String> currentPath = new Stack<>();
		
		Stack<List<WordWrapper>> tree = new Stack<>();
		Set<WordWrapper> allWords = preDivided.get(startWord.getWord().length());
		tree.push(findSingleDifferenceWords(startWord.getWord(), allWords));
		currentPath.push(startWord.getWord());
		long shortestPath = allWords.size();
		while(!tree.isEmpty()) {
			List<WordWrapper> list = tree.pop();
			if (list.isEmpty() || currentPath.size() > shortestPath) {
				currentPath.pop();
				continue;
			}
			WordWrapper word = list.remove(0);			
			if (word.getWord().equals(endWord)) {
				if (shortestPath > currentPath.size()) {
					solutions.clear();
				}
				shortestPath = currentPath.size();
				solutions.add(new ArrayList<>(currentPath));
				currentPath.pop();
				//there's no reason to look here anymore since possible combinations can only be longer
				continue;
			}
			tree.push(list);
			if (currentPath.contains(word)) {
				continue;
			}
			Set<WordWrapper> nextStep = word.getSingleDifferenceWords();
			if (nextStep == null) {
				List<WordWrapper> connected = findSingleDifferenceWords(word.getWord(), allWords);
				tree.push(connected);
				word.setSingleDifferenceWords(new HashSet<>(connected));
			} else {
				tree.push(new LinkedList<>(nextStep));
			}
			currentPath.push(word.getWord());
		}
		return solutions;
	}
	
	private List<WordWrapper> findSingleDifferenceWords(String word, Set<WordWrapper> collectionToSearch) {
		List<WordWrapper> response = new LinkedList<WordWrapper>();
		for (WordWrapper wrapper : collectionToSearch) {
			if (countLetterDifference(word, wrapper.getWord() ) == 1) {
				response.add(wrapper);
			}
		}
		return response;
	}
	
	private boolean validate(String startWord, String endWord) {
		if (StringUtils.isBlank(startWord) || StringUtils.isBlank(endWord) || startWord.length() != endWord.length()) {
			return false;
		}
		
        if (!dictSet.contains(startWord) ||  !dictSet.contains(endWord)) {
        	return false;
        }
        return true;
	}
	
    private int countLetterDifference(String first, String second) {
        int dif = 0;
        for (int i=0; i< first.length(); i++) {
            if (first.charAt(i) != second.charAt(i)) {
            	dif++;
            }
        }
        return dif;
    }
}
