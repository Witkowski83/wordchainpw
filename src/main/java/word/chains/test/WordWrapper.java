package word.chains.test;

import java.util.Set;

public final class WordWrapper {

	private final String word;
	private Set<WordWrapper> singleDifferenceWords;
	
	public WordWrapper(String word) {
		this.word = word;
	}

	public String getWord() {
		return word;
	}

	public Set<WordWrapper> getSingleDifferenceWords() {
		return singleDifferenceWords;
	}

	public void setSingleDifferenceWords(Set<WordWrapper> singleDifferenceWords) {
		this.singleDifferenceWords = singleDifferenceWords;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordWrapper other = (WordWrapper) obj;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
}
