package word.chains.test;

import java.util.List;

import org.junit.Test;

public class MagicalSolutionsBoxTest {
	
	@Test
	public void testfindAllCombinationsNonRecurse() {
    	MagicalSolutionsBox box = new MagicalSolutionsBox( App.loadDict(), 100 );
    	List<List<String>> combinations = box.findAllCombinationsNonRecurse("cat", "dog");

    	
    	//[cat, cot, cog], [cat, cot, dot], [cat, dat, dag], [cat, dat, dot]
    	System.out.println(combinations);
	}
}
