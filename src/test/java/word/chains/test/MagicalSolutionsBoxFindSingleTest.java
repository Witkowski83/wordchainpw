package word.chains.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class MagicalSolutionsBoxFindSingleTest {

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
	        return Arrays.asList(new Object[][]{
	                { null, "test", null },
	                { "test", null, null },
	                { null, null, null},
	                {"cat", "cat", Arrays.asList("cat")},
	                {"cat", "cats", null},
	                
	                {"cat", "dog", Arrays.asList("cat", "cot", "cog", "dog")},
	                {"lead", "gold", Arrays.asList("lead", "load", "goad", "gold")},
//	                {"ruby", "code", Arrays.asList("ruby", "rube", "robe", "rode", "code")}
	                {"ruby", "code", Arrays.asList("ruby", "rube", "rude", "rode", "code")}
	        });
	}
	 
	private String entryWord;
	private String endWord;	
	private List<String> solution;
	
	public MagicalSolutionsBoxFindSingleTest(String first, String second, List<String> answer) {
		entryWord = first;
		endWord = second;
		solution = answer;
	}
	
	@Test
	public void testFindWordChain() {
		MagicalSolutionsBox box = new MagicalSolutionsBox(App.loadDict(), 10);
		List<String> answer = box.findWordChain(entryWord, endWord);
		if (solution == null) {
			assertNull(answer);
		} else {
			assertNotNull(answer);
			assertEquals(solution.size(), answer.size());
			for (int i = 0; i < answer.size(); i++) {
				assertEquals(answer.get(i), solution.get(i));
			}
		}
	}
}
